# TP6 : Stockage et sauvegarde

# Partie 1 : Préparation de la machine `backup.tp6.linux`

Au menu :

- ajouter un disque dur à la VM
- créer une nouvelle partition sur le disque avec LVM
- formater la partition pour la rendre utilisable
- monter la partition pour la rendre accessible
- rendre le montage de la partition automatique, pour qu'elle soit toujours accessible


# I. Ajout de disque


🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**

```bash
[eymas@backup ~]$ lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```

# II. Partitioning


🌞 **Partitionner le disque à l'aide de LVM**

- créer un *physical volume (PV)* : le nouveau disque ajouté à la VM

```bash
[eymas@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for eymas:
  Physical volume "/dev/sdb" successfully created.
[eymas@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g
  ```
- créer un nouveau *volume group (VG)*
  - il devra s'appeler `backup`
  - il doit contenir le PV créé à l'étape précédente

```bash
[eymas@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
[eymas@backup ~]$
  ```

- créer un nouveau *logical volume (LV)* : ce sera la partition utilisable
  - elle doit être dans le VG `backup`
  - elle doit occuper tout l'espace libre

```bash
[eymas@backup ~]$ sudo lvcreate -l 100%FREE backup -n last_backup
  Calculated size of logical volume is 0 extents. Needs to be larger.
[eymas@backup ~]$ sudo lvs
  LV          VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_backup backup -wi-a-----  <5.00g
```

🌞 **Formater la partition**

- vous formaterez la partition en ext4 (avec une commande `mkfs`)
  - le chemin de la partition, vous pouvez le visualiser avec la commande `lvdisplay`
  - pour rappel un *Logical Volume (LVM)* **C'EST** une partition


```bash
[eymas@backup ~]$ sudo lvdisplay | grep backup | grep Path
  LV Path                /dev/backup/last_backup

[eymas@backup ~]$ sudo mkfs -t ext4 /dev/backup/last_backup
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: d5b87aff-cf78-4006-ae03-99c91781cb11
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

🌞 **Monter la partition**

- montage de la partition (avec la commande `mount`)
  - la partition doit être montée dans le dossier `/backup`
  - preuve avec une commande `df -h` que la partition est bien montée
  - prouvez que vous pouvez lire et écrire des données sur cette partition

```bash
[eymas@backup ~]$ sudo mkdir /backup

[eymas@backup ~]$ sudo mount /dev/backup/last_backup /backup

[eymas@backup ~]$ df -h
Filesystem                      Size  Used Avail Use% Mounted on
devtmpfs                        388M     0  388M   0% /dev
tmpfs                           405M     0  405M   0% /dev/shm
tmpfs                           405M  5.6M  400M   2% /run
tmpfs                           405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root             6.2G  2.1G  4.2G  34% /
/dev/sda1                      1014M  265M  750M  27% /boot
tmpfs                            81M     0   81M   0% /run/user/1000
/dev/mapper/backup-last_backup  4.9G   20M  4.6G   1% /backup
```
- définir un montage automatique de la partition (fichier `/etc/fstab`)
  - vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement

```bash
[eymas@backup ~]$ sudo nano /etc/fstab

#on colle la ligne 
/dev/backup/last_backup /backup ext4 defaults 0 0

[eymas@backup ~]$ sudo umount /backup

[eymas@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /backup does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/backup                  : successfully mounted
```
---

# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`


🌞 **Préparer les dossiers à partager**

```bash
[eymas@backup ~]$ cd /backup/

[eymas@backup backup]$ sudo mkdir web.tp6.linux
[eymas@backup backup]$ sudo mkdir db.tp6.linux

[eymas@backup backup]$ ls
db.tp6.linux  lost+found  web.tp6.linux
```

🌞 **Install du serveur NFS**

```bash
[eymas@backup ~]$ sudo dnf -y install nfs-utils
[...]
Complete!
```

🌞 **Conf du serveur NFS**

- fichier `/etc/idmapd.conf`

```bash
[eymas@backup etc]$ sudo nano /etc/idmapd.conf

#modification de la ligne domain par 
Domain = tp6.linux

[eymas@backup etc]$ cat /etc/idmapd.conf | grep Domain
Domain = tp6.linux
```

- fichier `/etc/exports`

```bash
[eymas@backup ~]$ sudo nano /etc/exports

#on ajoute les lignes:
/backup/web.tp6.linux 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.0/24(rw,no_root_squash)

[eymas@backup ~]$ cat /etc/exports
/backup/web.tp6.linux 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.0/24(rw,no_root_squash)
```

rw: veut dire que l'on peut ecrire dans le fichier et qu'on peut le lire car rw signifie read and write

no_root_squash: signifie que le répertoire laisse les droits root au root


🌞 **Démarrez le service**

- le service s'appelle `nfs-server`
- après l'avoir démarré, prouvez qu'il est actif

```bash
[eymas@backup ~]$ sudo systemctl start nfs-server

[eymas@backup ~]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Wed 2021-12-08 01:54:35 CET; 22s ago
  Process: 24533 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=e>
  Process: 24521 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 24520 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 24533 (code=exited, status=0/SUCCESS)

Dec 08 01:54:35 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 08 01:54:35 backup.tp6.linux systemd[1]: Started NFS server and services.
```

- faites en sorte qu'il démarre automatiquement au démarrage de la machine

```bash
[eymas@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

🌞 **Firewall**

- le port à ouvrir et le `2049/tcp`

```bash
[eymas@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
```
- prouvez que la machine écoute sur ce port (commande `ss`)

```bash
[eymas@backup ~]$ ss -alnpt | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```

# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

---

On commence par `web.tp6.linux`.

🌞 **Install'**

```bash
[eymas@web ~]$ sudo dnf install nfs-utils
[sudo] password for eymas:
Last metadata expiration check: 3:55:07 ago on Wed 01 Dec 2021 02:18:09 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                           Architecture           Version                          Repository              Size
========================================================================================================================
Installing:
 nfs-utils                         x86_64                 1:2.3.3-46.el8                   baseos                 499 k
Installing dependencies:
 gssproxy                          x86_64                 0.8.0-19.el8                     baseos                 118 k
 keyutils                          x86_64                 1.5.10-9.el8                     baseos                  65 k
 libevent                          x86_64                 2.1.8-5.el8                      baseos                 252 k
 libverto-libevent                 x86_64                 0.3.0-5.el8                      baseos                  15 k
 rpcbind                           x86_64                 1.2.5-8.el8                      baseos
[...]
Complete!
```

🌞 **Conf'**

- pareil que pour le serveur : fichier `/etc/idmapd.conf`

```bash
[eymas@web ~]$ sudo nano /etc/idmapd.conf

#on remplace la ligne domain par
Domain = tp6.linux

[eymas@web ~]$ cat /etc/idmapd.conf | grep tp6
Domain = tp6.linux
```

🌞 **Montage !**

- montez la partition NFS `/backup/web.tp6.linux/` avec une comande `mount`
 
```bash

```

- preuve avec une commande `df -h` que la partition est bien montée


```bash

```



- prouvez que vous pouvez lire et écrire des données sur cette partition


```bash

```



- définir un montage automatique de la partition (fichier `/etc/fstab`)
  - vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement
  

```bash

```


---

🌞 **Répétez les opérations sur `db.tp6.linux`**

- le point de montage sur la machine `db.tp6.linux` est aussi sur `/srv/backup`
- le dossier à monter est `/backup/db.tp6.linux/`
- vous ne mettrez dans le compte-rendu pour `db.tp6.linux` que les preuves de fonctionnement :
  - preuve avec une commande `df -h` que la partition est bien montée



```bash

```





  - preuve que vous pouvez lire et écrire des données sur cette partition





```bash

```




  - preuve que votre fichier `/etc/fstab` fonctionne correctement



```bash

```
---



# Partie 4 : Scripts de sauvegarde


## I. Sauvegarde Web

🌞 **Ecrire un script qui sauvegarde les données de NextCloud**


🌞 **Créer un service**


🌞 **Vérifier que vous êtes capables de restaurer les données**


🌞 **Créer un *timer***


## II. Sauvegarde base de données

🌞 **Ecrire un script qui sauvegarde les données de la base de données MariaDB**

---

🌞 **Créer un service**


🌞 **Créer un `timer`**


## Conclusion

Dans ce TP, plusieurs notions abordées :

- partitionnement avec LVM
- gestion de partitions au sens large
- partage de fichiers avec NFS
- scripting

Et à la fin ? Toutes les données de notre cloud perso sont sauvegardéééééééééééééééées. Le feu.
