# TP2 : Manipulation de services 

- [TP2 : Manipulation de services](#tp2--explorer-et-manipuler-le-système)
- [Intro](#intro)
- [Prérequis](#prérequis)
  - [1. Une machine xubuntu fonctionnelle](#1-une-machine-xubuntu-fonctionnelle)
  - [2. Nommer la machine](#2-nommer-la-machine)
  - [3. Config réseau](#3-config-réseau)
- [Go next](#go-next)

Dans nos cours, on ne va que peu s'attarder sur l'aspect client des systèmes GNU/Linux, mais plutôt sur la façon dont on le manipule en tant qu'admin.

Ca permettra aussi, *via* la manipulation, d'appréhender un peu mieux comment un OS de ce genre fonctionne.

# Intro

Dans ce TP on va s'intéresser aux *services* de la machine. Un *service* c'est un processus dont l'OS s'occupe. 

Plutôt que de le lancer à la main, on demande à l'OS de le gérer, c'est moins chiant !

> **Par exemple, quand vous ouvrez votre PC, vous lancez pas une commande pour avoir une interface graphique si ?** L'interface graphique, elle est juste là, elle pop "toute seule". En vérité, c'est l'OS qui l'a lancée. L'interface graphique est donc un *service*.

Souvent un service...

- bon bah c'est un processus qui s'exécute
  - c'est le système qui a fait en sorte qu'il se lance
  - le système a la charge du processus
  - genre il va le relancer si le processus crash par exemple
- souvent il a un fichier de configuration
  - ça nous permet de le paramétrer
  - les changements prennent effet quand on redémarre le service
- souvent on peut définir sous quelle identité le service va tourner
  - genre on désigne un utilisateur du système qui lancera le processus
  - c'est cet utilisateur qui s'affichera dans la liste des processus
- si c'est un service qui utilise le réseau (comme SSH, HTTP, FTP, autres.) il écoute sur un port

# Prérequis

> Y'a toujours une section prérequis dans mes TPs, ça vous sert à préparer l'environnement pour réaliser le TP. Dans ce TP, c'est le premier avec des prérequis, alors je vais détailler le principe et vous devrez me rendre la réalisation de ces étapes dans le compte rendu.

## 1. Une machine xubuntu fonctionnelle

N'hésitez pas à cloner celle qu'on a créé ensemble.

**Pour TOUTES les commandes que je vous donne dans le TP**

- elles sont dans le [memo de commandes Linux](../../cours/memos/commandes.md)
- vous **DEVEZ** consulter le `help` au minimum voire le `man` ou faire une recherche Internet pour comprendre comment fonctionne la commande

```bash
# Consulter le help de ls
$ ls --help

# Consulter le manuel de ls
$ man ls
```

## 2. Nommer la machine

➜ **On va renommer la machine**

- parce qu'on s'y retrouve plus facilement
- parce que toutes les machines sont nommées dans la vie réelle, pour cette raison, alors habituez vous à le faire systématiquement :)

On désignera la machine par le nom `node1.tp2.linux`

🌞 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**

```bash
emays@eymas-pc:~$ sudo hostname node1.tp2.linux
```

- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**

```bash
sudo nano /etc/hostname
```
  
## 3. Config réseau

➜ **Vérifiez avant de continuer le TP que la configuration réseau de la machine est OK. C'est à dire :**

- la machine doit pouvoir joindre internet
- votre PC doit pouvoir `ping` la machine

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash
# Affichez la liste des cartes réseau de la machine virtuelle
# Vérifiez que les cartes réseau ont toute une IP
$ ip a

# Si les cartes n'ont pas d'IP vous pouvez les allumez avec la commande
$ nmcli con up <NOM_INTERFACE>
# Par exemple
$ nmcli con up enp0s3

# Vous devez repérer l'adresse de la VM dans le host-only

# Vous pouvez tester de ping un serveur connu sur internet
# On teste souvent avec 1.1.1.1 (serveur DNS de CloudFlare)
# ou 8.8.8.8 (serveur DNS de Google)
$ ping 1.1.1.1

# On teste si la machine sait résoudre des noms de domaine
$ ping ynov.com
```

Ensuite on vérifie que notre PC peut `ping` la machine (étapes à réaliser SUR VOTRE PC) :

```bash
# Afficher la liste de vos carte réseau, la commande dépend de votre OS
$ ip a # Linux
$ ipconfig # Windows
$ ifconfig # MacOS

# Vous devez repérer l'adresse de votre PC dans le host-only

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>
```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  - depuis la VM : `ping 1.1.1.1` fonctionnel
```cash
emays@eymas-node1:~$ ping 1.1.1.1 -c 2
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=24.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=23.1 ms

--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 23.137/23.999/24.862/0.826 ms
```
  - depuis la VM : `ping ynov.com` fonctionnel
```bash
emays@eymas-node1:~$ ping ynov.com -c 2
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=23.0 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=50 time=25.1 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 22.984/24.053/25.123/1.069 ms
```
  - depuis votre PC : `ping <IP_VM>` fonctionnel
```bash
C:\Users\eymas>ping 192.168.56.113

Envoi d’une requête 'Ping'  192.168.56.113 avec 32 octets de données :
Réponse de 192.168.56.113 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.113 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.113 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.113 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.56.113:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
.                        

.

.

.

.


# Partie 1 : SSH

**Partie 1 : SSH**

I. Intro

II. Setup du serveur SSH

1. *Installation du serveur*
2. *Lancement du service SSH*
3. *Etude du service SSH*
4. *Modification de la configuration du serveur*




I. Intro


II. Setup du serveur SSH



1. **Installation du serveur**

🌞 Installer le paquet openssh-server

```bash
eymas@eymas-node1:~$ sudo apt install
[sudo] password for eymas:
Reading package lists... Done
Building dependency tree
Reading state information... Done
0 upgraded, 0 newly installed, 0 to remove and 89 not upgraded.
```





2. **Lancement du service SSH**

🌞 Lancer le service ssh

```bash
eymas@eymas-node1:~$ systemctl start sshd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'ssh.service'.
Authenticating as: eymas,,, (eymas)
Password:
==== AUTHENTICATION COMPLETE ===
```

```bash
eymas@eymas-node1:~$ systemctl status sshd
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 18:20:56 CET; 53min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 568 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 586 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 4.0M
     CGroup: /system.slice/ssh.service
             └─586 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

nov. 04 18:20:31 eymas-node1.tp2.linux systemd[1]: Starting OpenBSD Secure Shell server...
nov. 04 18:20:56 eymas-node1.tp2.linux sshd[586]: Server listening on 0.0.0.0 port 22.
nov. 04 18:20:56 eymas-node1.tp2.linux sshd[586]: Server listening on :: port 22.
nov. 04 18:20:56 eymas-node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
nov. 04 19:12:28 eymas-node1.tp2.linux sshd[1756]: Accepted password for eymas from 192.168.56.1 port 54605 ssh2
nov. 04 19:12:28 eymas-node1.tp2.linux sshd[1756]: pam_unix(sshd:session): session opened for user eymas by (uid=0)
eymas@eymas-node1:~$ systemctl status
```


3. **Etude du service SSH**

🌞 Analyser le service en cours de fonctionnement

afficher le statut du service

```bash
eymas@eymas-node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 18:20:56 CET; 3 days ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 586 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 4.1M
     CGroup: /system.slice/ssh.service
             └─586 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```


afficher le/les processus liés au service ssh

```bash
eymas@eymas-node1:~$ ps -ef | grep sshd
root         586       1  0 nov.06 ?       00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root       17586     586  0 14:19 ?        00:00:00 sshd: eymas [priv]
eymas      17674   17586  0 14:19 ?        00:00:00 sshd: eymas@pts/1
eymas      17769   17675  0 14:54 pts/1    00:00:00 grep --color=auto sshd
```


afficher le port utilisé par le service ssh

```bash
eymas@eymas-node1:~$ ss -l | grep ssh
u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh 27176                                           * 0
u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 27738                                           * 0
u_str   LISTEN   0        128                   /tmp/ssh-f57jJWVcr52v/agent.931 27463                                           * 0
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       0.0.0.0:*
tcp     LISTEN   0        128                                              [::]:ssh                                          [::]:*
```


afficher les logs du service ssh

```bash
eymas@eymas-node1:~$ cd /var/log/
```
```bash
eymas@eymas-node1:/var/log$ cat auth.log | grep sshd

Nov  5 12:29:01 eymas-node1 sshd[1756]: pam_unix(sshd:session): session closed for user eymas
Nov  8 14:19:25 eymas-node1 sshd[17586]: Accepted password for eymas from 192.168.56.1 port 51279 ssh2
Nov  8 14:19:25 eymas-node1 sshd[17586]: pam_unix(sshd:session): session opened for user eymas by (uid=0)
```




🌞 Connectez vous au serveur

```bash
C:\Users\eymas>ssh eymas@192.168.56.113
eymas@192.168.56.113's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

77 updates can be applied immediately.
16 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Mon Nov  8 14:19:25 2021 from 192.168.56.1
```


4. Modification de la configuration du serveur

🌞 Modifier le comportement du service

```bash
eymas@eymas-node1:~$ sudo nano /etc/ssh/sshd_config
```


```bash
eymas@eymas-node1:~$ cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.


Include /etc/ssh/sshd_config.d/*.conf

Port 23456
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

```bash
eymas@eymas-node1:~$ systemctl restart sshd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to restart 'ssh.service'.
Authenticating as: eymas,,, (eymas)
Password:
==== AUTHENTICATION COMPLETE ===
```

```bash
eymas@eymas-node1:~$ ss -l
Netid State  Recv-Q Send-Q                              Local Address:Port                  Peer Address:Port   Process
tcp   LISTEN 0      128                                          [::]:23456                         [::]:*
```

🌞 Connectez vous sur le nouveau port choisi

```bash
C:\Users\eymas>ssh eymas@192.168.56.113 -p 23456
eymas@192.168.56.113's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

77 updates can be applied immediately.
16 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Mon Nov  8 16:26:59 2021 from 192.168.56.1
```


# Partie 2 : FTP

**Partie 2 : FTP**

I. Intro

II. Setup du serveur FTP

1. Installation du serveur
2. Lancement du service FTP
3. Etude du service FTP
4. Modification de la configuration du serveur





1. Installation du serveur

🌞 Installer le paquet vsftpd

```bash
eymas@eymas-node1:~$ sudo apt install vsftpd
[sudo] password for eymas:
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 77 not upgraded.
Need to get 115 kB of archives.
After this operation, 338 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu focal/main amd64 vsftpd amd64 3.0.3-12 [115 kB]
Fetched 115 kB in 0s (532 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 199594 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-12_amd64.deb ...
Unpacking vsftpd (3.0.3-12) ...
Setting up vsftpd (3.0.3-12) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for systemd (245.4-4ubuntu3.11) ...
```

2. Lancement du service FTP

🌞 Lancer le service vsftpd

```bash
eymas@eymas-node1:~$ sudo systemctl start vsftpd
eymas@eymas-node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 23:22:43 CET; 2min 31s ago
   Main PID: 18747 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 528.0K
     CGroup: /system.slice/vsftpd.service
             └─18747 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```


3. Etude du service FTP

🌞 Analyser le service en cours de fonctionnement

afficher le statut du service

```bash
eymas@eymas-node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 23:22:43 CET; 4min 7s ago
   Main PID: 18747 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 528.0K
     CGroup: /system.slice/vsftpd.service
             └─18747 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```


afficher le/les processus liés au service vsftpd

```bash
eymas@eymas-node1:~$ ps -ef
root       18747       1  0 23:22 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd.conf
```


afficher le port utilisé par le service vsftpd

```bash
eymas@eymas-node1:~$ ss -lntr
State       Recv-Q      Send-Q           Local Address:Port            Peer Address:Port     Process
LISTEN      0           4096              localhost%lo:53                   0.0.0.0:*
LISTEN      0           5                    localhost:631                  0.0.0.0:*
LISTEN      0           128                    0.0.0.0:23456                0.0.0.0:*
LISTEN      0           32                           *:21                         *:*
LISTEN      0           5                ip6-localhost:631                     [::]:*
LISTEN      0           128                       [::]:23456                   [::]:*
```


afficher les logs du service vsftpd

```bash
eymas@eymas-node1:~$ journalctl -xe -u vsftpd
-- Logs begin at Tue 2021-10-19 16:28:07 CEST, end at Mon 2021-11-08 23:35:21 CET. --
nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
-- Subject: A start job for unit vsftpd.service has begun execution
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- A start job for unit vsftpd.service has begun execution.
--
-- The job identifier is 24394.
nov. 08 23:22:43 eymas-node1.tp2.linux systemd[1]: Started vsftpd FTP server.
-- Subject: A start job for unit vsftpd.service has finished successfully
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- A start job for unit vsftpd.service has finished successfully.
--
-- The job identifier is 24394.
```





🌞 Connectez vous au serveur

```bash
eymas@eymas-node1:~$ ls
'Local Settings' Desktop  Documents  Downloads  eliott  Favorites  Music  Pictures  Public  Templates  Videos  vsftpd.conf
```

🌞 Visualiser les logs

```bash
Statut :	Contenu du dossier "/home/eymas" affiché avec succès
Statut :	Déconnecté du serveur
Statut :	Déconnecté du serveur
Statut :	Connexion interrompue par le serveur
```


🌞 Modifier le comportement du service

```bash
eymas@eymas-node1:~$ sudo nano /etc/vsftpd.conf
eymas@eymas-node1:~$ cat /etc/vsftpd.conf
listen_port=20000
eymas@eymas-node1:~$ ss -lntr
State       Recv-Q      Send-Q           Local Address:Port             Peer Address:Port      Process
LISTEN      0           4096              localhost%lo:53                    0.0.0.0:*
LISTEN      0           5                    localhost:631                   0.0.0.0:*
LISTEN      0           128                    0.0.0.0:23456                 0.0.0.0:*
LISTEN      0           32                           *:34567                       *:*
LISTEN      0           5                ip6-localhost:631                      [::]:*
LISTEN      0           128                       [::]:23456                    [::]:*
```

🌞 Connectez vous sur le nouveau port choisi

```bash
```



# Partie 3 : Création de votre propre service

**Partie 3 : Création de votre propre service**

I. Intro

II. Jouer avec netcat

III. Un service basé sur netcat

1. Créer le service
2. Test test et retest


🌞 Donnez les deux commandes pour établir ce petit chat avec netcat


🌞 Utiliser netcat pour stocker les données échangées dans un fichier


🌞 Créer un nouveau service


🌞 Tester le nouveau service

