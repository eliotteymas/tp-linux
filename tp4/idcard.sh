```bash
nom_machine=$(hostname)
os_name=$(cat /etc/os-release | head -1 | cut -d '"' -f2)
version_noyau=$(cat /proc/version | cut -d " " -f3)
IP_machine=$(hostname -i | cut -d " " -f2)
ram_dispo=$(free -mh | grep Mem | cut -d " " -f46)
ram_total=$(free -mh | grep Mem | cut -d " " -f11)
disquedur_restant=$(df -h | grep /dev/sda5 | cut -d " " -f12)
ram_processe1=$(ps -o %mem,command ax | sort -r -b | head -2 | tail -1)
ram_processe2=$(ps -o %mem,command ax | sort -r -b | head -3 | tail -1)
ram_processe3=$(ps -o %mem,command ax | sort -r -b | head -4 | tail -1)
ram_processe4=$(ps -o %mem,command ax | sort -r -b | head -5 | tail -1)
ram_processe5=$(ps -o %mem,command ax | sort -r -b | head -6 | tail -1)
prt_listen=$(sudo ss -tunlp | grep LISTEN | cut -c52-58,74,80-150)
gif_chat=$(curl curl https://api.thecatapi.com/v1/images/search | cut -d '"' -f10)

echo "Machine Name : $nom_machine"
echo "OS $os_name and kernel version is $version_noyau"
echo "IP : $IP_machine"
echo "RAM : $ram_dispo RAM restante sur $ram_total RAM totale"
echo "Disque : $disquedur_restant space left"
echo "Top 5 processes by RAM usage :"
echo "- $ram_processe1"
echo "- $ram_processe2"
echo "- $ram_processe3"
echo "- $ram_processe4"
echo "- $ram_processe5"
echo "Listening ports :"
echo "- 22 : sshd"
echo "- $prt_listen"
echo "Here's your random cat : $gif_chat"
```
