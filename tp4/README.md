# TP4 : Une distribution orientée serveur

# Sommaire

- [TP4 : Une distribution orientée serveur](#tp4--une-distribution-orientée-serveur)
- [Sommaire](#sommaire)
- [I. Install de Rocky Linux](#i-install-de-rocky-linux)
- [II. Checklist](#ii-checklist)
- [III. Mettre en place un service](#iii-mettre-en-place-un-service)
  - [1. Intro NGINX](#1-intro-nginx)
  - [2. Install](#2-install)
  - [3. Analyse](#3-analyse)
  - [4. Visite du service web](#4-visite-du-service-web)
  - [5. Modif de la conf du serveur web](#5-modif-de-la-conf-du-serveur-web)

# I. Install de Rocky Linux


# II. Checklist


🌞 **Choisissez et définissez une IP à la VM**

le contenu du fichier conf:
```bash
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.88
NETMASK=255.255.255.0
```
résultat d'un ip a:

```bash
[eymas@localhost network-scripts]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ed:5e:27 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.88/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feed:5e27/64 scope link
       valid_lft forever preferred_lft forever
````

---

🌞 **Vous me prouverez que :**

service ssh actif sur la VM
```bash
[eymas@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2021-11-23 16:32:36 CET; 46min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 849 (sshd)
    Tasks: 1 (limit: 4944)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
```
échange de clés

```bash
[eymas@localhost ~]$ mkdir ssh
[eymas@localhost ~]$ cd ssh
[eymas@localhost ssh]$ touch authorized_keys
[eymas@localhost ssh]$ nano authorized_keys
[eymas@localhost ssh]$ chmod 600 authorized_keys
[eymas@localhost ssh]$ cd ..
[eymas@localhost ~]$ chmod 700 ssh/
[eymas@localhost ~]$ exit
```

---


🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`

```bash
[eymas@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=3.86 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=4.65 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=4.44 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 3.863/4.320/4.654/0.338 ms
```

🌞 **Prouvez que vous avez de la résolution de nom**

Un petit `ping` vers un nom de domaine, celui que vous voulez :)

```bash
[eymas@localhost ~]$ ping www.supremenewyork.com
PING supremenewyork.map.fastly.net (151.101.122.133) 56(84) bytes of data.
64 bytes from 151.101.122.133 (151.101.122.133): icmp_seq=1 ttl=56 time=25.5 ms
64 bytes from 151.101.122.133 (151.101.122.133): icmp_seq=2 ttl=56 time=24.6 ms
64 bytes from 151.101.122.133 (151.101.122.133): icmp_seq=3 ttl=56 time=24.8 ms
64 bytes from 151.101.122.133 (151.101.122.133): icmp_seq=4 ttl=56 time=25.1 ms
^C
--- supremenewyork.map.fastly.net ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 24.632/25.005/25.481/0.318 ms
```
---

➜ **Nommage de la machine**


🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

```bash
[eymas@localhost ~]$ cat /etc/hostname
node1.tp4.linux
```
```bash
[eymas@localhost ~]$ hostname
node1.tp4.linux
```

# III. Mettre en place un service


## 1. Intro NGINX


## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**

```bash
[eymas@localhost ~]$ sudo dnf install nginx
[...]
Is this ok [y/N]: y
[...]
Is this ok [y/N]: y
[...]
Complete!
```

## 3. Analyse

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX

```bash
[eymas@localhost ~]$ ps -ef | grep nginx
root       26245       1  0 03:04 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      26246   26245  0 03:04 ?        00:00:00 nginx: worker process
eymas      26254    1972  0 03:05 pts/0    00:00:00 grep --color=auto nginx
```
- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web

```bash
[eymas@localhost ~]$ ss -alnpt
State         Recv-Q         Send-Q                 Local Address:Port                 Peer Address:Port        Process
LISTEN        0              128                          0.0.0.0:80                        0.0.0.0:*
LISTEN        0              128                          0.0.0.0:22                        0.0.0.0:*
LISTEN        0              128                             [::]:80                           [::]:*
LISTEN        0              128                             [::]:22
```
- en regardant la conf, déterminer dans quel dossier se trouve la racine web

```bash
[eymas@localhost ~]$ cat /etc/nginx/nginx.conf | grep root
        root         /usr/share/nginx/html;
#        root         /usr/share/nginx/html;
```
- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus

```bash
[eymas@localhost ~]$ ls -al /usr/share/nginx/html/
total 20
drwxr-xr-x. 2 root root   99 Nov 24 02:59 .
drwxr-xr-x. 4 root root   33 Nov 24 02:59 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```

## 4. Visite du service web

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** (c'est du TCP ;) )

```bash
[eymas@localhost ~]$ sudo firewall-cmd --add-port=88/tcp --permanent
[sudo] password for eymas:
success
```
```bash
[eymas@localhost ~]$ sudo firewall-cmd --reload
success
```

🌞 **Tester le bon fonctionnement du service**

```bash
```

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

```bash
```

```bash
```
---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`
- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur

---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
  - avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
  - le dossier et tout son contenu doivent appartenir à `web`
- configurez NGINX pour qu'il utilise cette nouvelle racine web
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

![Uuuuunpossible](./pics/nginx_unpossible.jpg)
