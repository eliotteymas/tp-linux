# TP 3 : A little script

- [TP 3 : A little script](#tp-3--a-little-script)
- [Intro](#intro)
- [I. Script carte d'identité](#i-script-carte-didentité)
  - [Rendu](#rendu)
- [II. Script youtube-dl](#ii-script-youtube-dl)
  - [Rendu](#rendu-1)
- [III. MAKE IT A SERVICE](#iii-make-it-a-service)
  - [Rendu](#rendu-2)
- [Bonus](#iv-bonus)

# Intro


# I. Script carte d'identité

Ce que doit faire le script. Il doit afficher :

- le nom de la machine
- le nom de l'OS de la machine
- la version du noyau Linux utilisé par la machine
- l'adresse IP de la machine
- l'état de la RAM
  - espace dispo en RAM (en Go, Mo, ou Ko)
  - taille totale de la RAM (en Go, Mo, ou ko)
- l'espace restant sur le disque dur, en Go (ou Mo, ou ko)
- le top 5 des processus qui pompent le plus de RAM sur la machine actuellement. Procédez par étape :
  - listez les process
  - affichez la RAM utilisée par chaque process
  - triez par RAM utilisée
  - isolez les 5 premiers
- la liste des ports en écoute sur la machine, avec le programme qui est derrière
- un lien vers une image/gif random de chat
  - il y a de très bons sites pour ça hihi
  - avec [celui-ci](https://docs.thecatapi.com/), une simple commande `curl https://api.thecatapi.com/v1/images/search` vous retourne l'URL d'une random image de chat

## Rendu

📁 **Fichier `/srv/idcard/idcard.sh`**

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```bash
eymas@eymas-node1:/srv/idcard$ sh /srv/idcard/idcard.sh

Machine Name : eymas-node1.tp2.linux
OS Ubuntu and kernel version is 5.11.0-38-generic
IP : 192.168.56.113
RAM : 1,3Gi RAM restante sur 1,9Gi RAM totale
Disque : 2,0G space left
Top 5 processes by RAM usage :
-  4.0 xfwm4 --replace
-  3.4 /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
-  2.2 /usr/bin/python3 /usr/bin/blueman-applet
-  2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libwhiskermenu.so 1 16777223 whiskermenu Whisker Menu Show a menu to easily access installed applications
-  1.9 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libpulseaudio-plugin.so 10 16777229 pulseaudio PulseAudio Plugin Adjust the audio volume of the PulseAudio sound system
Listening ports :
- 22 : sshd
- 53     :  users:(("systemd-resolve",pid=400,fd=13))
631    :  users:(("cupsd",pid=521,fd=7))
23456  :  users:(("sshd",pid=582,fd=3))
631    :  users:(("cupsd",pid=521,fd=6))
23456  :  users:(("sshd",pid=582,fd=4))
9999   :  users:(("vsftpd",pid=584,fd=3))
Here's your random cat :
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>

</body></html>
https://cdn2.thecatapi.com/images/9q9.jpg
```

# II. Script youtube-dl


➜ **1. Permettre le téléchargement d'une vidéo youtube dont l'URL est passée au script**

- la vidéo devra être téléchargée dans le dossier `/srv/yt/downloads/`
  - le script doit s'assurer que ce dossier existe sinon il quitte
  - vous pouvez utiliser la commande `exit` pour que le script s'arrête
- plus précisément, chaque téléchargement de vidéo créera un dossier
  - `/srv/yt/downloads/<NOM_VIDEO>`
  - il vous faudra donc, avant de télécharger la vidéo, exécuter une commande pour récupérer son nom afin de créer le dossier en fonction
- la vidéo sera téléchargée dans
  - `/srv/yt/downloads/<NOM_VIDEO>/<NOM_VIDEO>.mp4`
- la description de la vidéo sera aussi téléchargée
  - dans `/srv/yt/downloads/<NOM_VIDEO>/description`
  - on peut récup la description avec une commande `youtube-dl`
- la commande `youtube-dl` génère du texte dans le terminal, ce texte devra être masqué
  - vous pouvez utiliser une redirection de flux vers `/dev/null`, c'est ce que l'on fait généralement pour se débarasser d'une sortie non-désirée

➜ **2. Le script produira une sortie personnalisée**

➜ **3. A chaque vidéo téléchargée, votre script produira une ligne de log dans le fichier `/var/log/yt/download.log`**


## Rendu

📁 **Le script `/srv/yt/yt.sh`**

📁 **Le fichier de log `/var/log/yt/download.log`**, avec au moins quelques lignes

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```bash
eymas@eymas-VirtualBox:~$ sudo bash /srv/yt/yt.sh https://www.youtube.com/watch?v=KEsPZ5JEEL4
Video https://https://www.youtube.com/watch?v=KEsPZ5JEEL4 was downloaded.
File path : /srv/yt/downloads/Vidéo Courte et Drôle :) | #26/Vidéo Courte et Drôle :) | #26.mp4
```

# III. MAKE IT A SERVICE

## Rendu

📁 **Le script `/srv/yt/yt-v2.sh`**

📁 **Fichier `/etc/systemd/system/yt.service`**

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :

- un `systemctl status yt` quand le service est en cours de fonctionnement


- un extrait de `journalctl -xe -u yt`


- la commande qui permet à ce service de démarrer automatiquement quand la machine démarre


