# I. Setup DB

## Sommaire

- [I. Setup DB](#i-setup-db)
  - [Sommaire](#sommaire)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)

## 1. Install MariaDB


🌞 **Installer MariaDB sur la machine `db.tp5.linux`**

```apache
[eymas@db ~]$ sudo dnf install mariadb-server
```

🌞 **Le service MariaDB**

```apache
[eymas@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
[...]
    Active: inactive (dead)
```
```apache
[eymas@db ~]$ systemctl start mariadb
```
```apache
[eymas@db ~]$ sudo systemctl enable mariadb
```
```apache
[eymas@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
[...]
Active: active (running) since Thu 2021-11-25 20:49:17 CET; 6s ago
```
```apache
[eymas@db ~]$ sudo ss -laputen | grep mysqld
tcp   LISTEN 0      80                    *:3306            *:*     users:(("mysqld",pid=4767,fd=21)) uid:27 ino:34025 sk:4c v6only:0 <->
```
```apache
[eymas@db ~]$ ps aux | grep mysql
mysql       4767  0.0 11.0 1296832 91376 ?       Ssl  20:49   0:00 /usr/libexec/mysqld --based
```

🌞 **Firewall**

```apache
[eymas@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for eymas:
success
[eymas@db ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB


🌞 **Configuration élémentaire de la base**

```apache
[eymas@db ~]$ sudo mysql_secure_installation
```

Cette question me demande si je veux définir un mot de passe root.

```apache
Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

```
Cette question me demande de rentrer un mot de passe root

```apache
Remove anonymous users? [Y/n] Y
... Success!
```
Cette question me demande si je veux désactiver la connexion sans compte utilisateurs

```bash
Disallow root login remotely? [Y/n] Y
 ... Success!
```
Cette question me demande si je veux la connexion root à distance
```bash
Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
```
Cette question me demande si je veux désactiver le database ("test")

```bash
Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
Cette question me demande de redémarrer la table des privilèges pour mettre en route les changements


🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

```bash
[eymas@db ~]$ sudo mysql -u root -p
[sudo] password for thomas:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

## 3. Test

Bon, là il faut tester que la base sera utilisable par NextCloud.


🌞 **Installez sur la machine `web.tp5.linux` la commande `mysql`**

- vous utiliserez la commande `dnf provides` pour trouver dans quel paquet se trouve cette commande

```apache
[eymas@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                               3.8 MB/s | 8.2 MB     00:02
Rocky Linux 8 - BaseOS                                                                  2.9 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                                                                   23 kB/s |  10 kB     00:00
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```
```apache
[eymas@web ~]$ sudo dnf install mysql
[sudo] password for eymas:
Last metadata expiration check: 9:07:40 ago on Thu 25 Nov 2021 08:45:48 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture   Version                                      Repository         Size
========================================================================================================================
Installing:
 mysql                              x86_64         8.0.26-1.module+el8.4.0+652+6de068a7         appstream          12 M
Installing dependencies:
 mariadb-connector-c-config         noarch         3.1.11-2.el8_3                               appstream          14 k
 mysql-common                       x86_64         8.0.26-1.module+el8.4.0+652+6de068a7         appstream         133 k
Enabling module streams:
 mysql                                             8.0

Transaction Summary
========================================================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
```


🌞 **Tester la connexion**

- utilisez la commande `mysql` depuis `web.tp5.linux` pour vous connecter à la base qui tourne sur `db.tp5.linux`

```apache
[eymas@web ~]$ mysql --host=10.5.1.12 --user=nextcloud --port=3306 -p --database=nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 29
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server
```

 Je veux donc dans le compte-rendu la commande `mysql` qui permet de se co depuis `web.tp5.linux` au service de base de données qui tourne sur `db.tp5.linux`, ainsi que le `SHOW TABLES`.

```apache
mysql> SHOW TABLES;
Empty set (0.00 sec)
```

C'est bon ? Ca tourne ? [**Go installer NextCloud maintenant !**](./web.md)


# II. Setup Web


## Sommaire

- [II. Setup Web](#ii-setup-web)
  - [Sommaire](#sommaire)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
  - [3. Install NextCloud](#3-install-nextcloud)
  - [4. Test](#4-test)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp5.linux`**

```apache
[eymas@web ~]$ sudo dnf install httpd
[...]
Complete!
```

---

🌞 **Analyse du service Apache**

- lancez le service `httpd` et activez le au démarrage
```apache
[eymas@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: eymas
Password:
==== AUTHENTICATION COMPLETE ====
```
- isolez les processus liés au service `httpd`
```apache
[eymas@web ~]$ sudo ss -laputen | grep httpd
tcp   LISTEN 0      128                   *:80              *:*     users:(("httpd",pid=2846,fd=4),("httpd",pid=2845,fd=4),("httpd",pid=2844,fd=4),("httpd",pid=2842,fd=4)) ino:33444 sk:4 v6only:0 <->
```
- déterminez sur quel port écoute Apache par défaut
- déterminez sous quel utilisateur sont lancés les processus Apache

```apache
[eymas@web ~]$ ps aux
[...]
apache      2843  0.0  1.0 296820  8564 ?        S    06:10   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2844  0.0  1.4 1354604 12224 ?       Sl   06:10   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2845  0.0  1.7 1485732 14272 ?       Sl   06:10   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2846  0.0  1.4 1354604 12224 ?       Sl   06:10   0:00 /usr/sbin/httpd -DFOREGROUND
[...]
```

---

🌞 **Un premier test**

- ouvrez le port d'Apache dans le firewall

```apache
[eymas@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for eymas:
success
```
```apache
[eymas@web ~]$ sudo firewall-cmd --reload
success
```
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
  - avec une commande `curl`
  - avec votre navigateur Web

```apache
[eymas@web ~]$ curl www.apache.com
<!DOCTYPE html>
<!--[if lt IE 7 ]>      <html lang="en-US" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>         <html lang="en-US" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>         <html lang="en-US" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>         <html lang="en-US" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-US" class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8" />
[...]
```

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**


# ajout des dépôts EPEL
$ sudo dnf install epel-release

```apache
[eymas@web ~]$ sudo dnf install epel-release
[sudo] password for eymas:
Last metadata expiration check: 9:41:26 ago on Thu 25 Nov 2021 08:45:48 PM CET.
Dependencies resolved.
===================================================================================================
 Package                    Architecture         Version                Repository            Size
===================================================================================================
Installing:
 epel-release               noarch               8-13.el8               extras                23 k

Transaction Summary
===================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
[...]
Complete!
```
$ sudo dnf update

```apache
[eymas@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                     2.9 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64             1.4 MB/s | 958 kB     00:00
Last metadata expiration check: 0:00:01 ago on Fri 26 Nov 2021 06:30:41 AM CET.
Dependencies resolved.
Nothing to do.
Complete!
```
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

```apache
[eymas@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:59 ago on Fri 26 Nov 2021 06:30:41 AM CET.
remi-release-8.rpm                                                 102 kB/s |  26 kB     00:00
Dependencies resolved.
===================================================================================================
 Package                 Architecture      Version                   Repository               Size
===================================================================================================
Installing:
 remi-release            noarch            8.5-1.el8.remi            @commandline             26 k

Transaction Summary
===================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 20 k
Is this ok [y/N]: y
[...]
Complete!
```
$ dnf module enable php:remi-7.4

```apache
[eymas@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64          2.4 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64          3.0 MB/s | 3.1 kB     00:00
[...]
Complete!
```

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

```apache
[eymas@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:02:51 ago on Fri 26 Nov 2021 06:33:28 AM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
[...]
Complete!
```
.

.

.


# Bonsoir monsieur je n'ai pas compris la fin la du tp et je viens deja de dépasser l'heure c'est pour cela qu'il n'est pas complet :/ désolé.


## 2. Conf Apache

---

🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in*

🌞 **Créer un VirtualHost qui accueillera NextCloud**

- créez un nouveau fichier dans le dossier de *drop-in*
  - attention, il devra être correctement nommé (l'extension) pour être inclus par le fichier de conf principal
- ce fichier devra avoir le contenu suivant :


🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache (commande `chown`, voir le [mémo commandes](../../cours/memos/commandes.md))

🌞 **Configurer PHP**



## 3. Install NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**



🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
- déplacer tout le contenu dans la racine Web
  - n'oubliez pas de gérer les permissions de tous les fichiers déplacés ;)
- supprimer l'archive

## 4. Test

---

🌞 **Modifiez le fichier `hosts` de votre PC**

- ajoutez la ligne : `10.5.1.11 web.tp5.linux`

🌞 **Tester l'accès à NextCloud et finaliser son install'**

---
