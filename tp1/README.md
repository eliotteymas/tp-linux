# TP1 : Are you dead yet ?

---

**🌞 Voici mes multiples façons d'exploser un linux :**

➜ **Première Technique **: 

Supprimer tous les dossiers racines :

```  
eymas@eymas-pc:~$ rm -rf / --no-preserve-root
```

➜ **Deuxième Technique **: 

Relancer à l'infini la saisi du mot de passe de la vm a chaque fois le mot de passe entré : 

Il faut d'abbord aller dans :
        - Session and Startup
        - Application Autostart
        - Add
        - command : shutdown now
        - Ok

```
```

➜ **Troisième Technique **: 

Lancement de terminal a l'infini aprés le premier lancement du terminal et désactiver la touche alt:

Il faut d'abbord aller dans :
        - Session and Startup
        - Application Autostart
        - Add
        - command : exo-open --launch TerminalEmulator
        - Ok
        
Puis :
        - Home
        - View
        - Cocher la case Show Hidden Files
        - Cliquer sur .bashrc
        - Ecrire exo-open --launch TerminalEmulator et xmodmap -e 'keycode 64='


```
```

➜ **Quatrième Technique **: 

Désactiver tout le clavier : 

Il faut d'abbord aller dans : 
        - Home
        - View
        - Cocher la case Show Hidden Files
        - Cliquer sur .bashrc
        - Ecrire xmodmap -e 'keycode x='  sur 65 lignes en remplaçant le x par les chiffres allant de 1 à 65


```
```

➜ **Cinquième Technique **: 

Remplire entierement la ram de la machine :

Il faut d'abbord cliquer sur : 
        - Session and Startup
        - Application Autostart
        - Add
        - command : exo-open --launch TerminalEmulator

Puis dans le terminal : 

```
eymas@eymas-pc:~$ sudo nano ~/.bashrc
```
